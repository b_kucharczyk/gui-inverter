import sys, builtins

from classes.inv_control import InvControl
from classes.inv_control_canopen import InvControlCanopen
from classes.inv_control_acs import InvControlAcs
from classes.inv_frontend import InvFrontend
from classes.macro_map import MacroMapDialog
from classes.inv_torqueAngleConfig import TorqueAngleConfig

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QHBoxLayout, QDoubleSpinBox

builtins.APP_NAME = "GUI Inverter"
APP_NAME = "GUI Inverter"

from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QVBoxLayout, \
        QComboBox, QLabel

from PyQt5.QtWidgets import QAction

invChooseList = {
    0: {
        "title":    "(brak)",
        "obj":      None
    },
    #1: {
    #    "title":    "Symulator",
    #    "obj":      InvControl
    #},
    1: {
        "title":    "CANopen",
        "obj":      InvControlCanopen
    },
    2: {
        "title":    "Modbus RTU",
        "obj":      InvControl
    },
    3: {
        "title":    "ACS 800 Modbus TCP",
        "obj":      InvControlAcs
    }
}

class InvChooser(QDialog):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        self.setWindowTitle(APP_NAME)
        layout = QVBoxLayout()
        
        cbList  = dict()

        for i in range(2):
            cbInv   = QComboBox()
            cbPort  = QComboBox()
            cbPort.setEnabled(False)
            
            for n in range(len(invChooseList)):
                cbInv.addItem(invChooseList[n].get("title"))

            cbInv.currentIndexChanged.connect(self.__cbChanged)
            cbInv.cbPort  = cbPort
            cbList[i] = cbInv

            hl = QHBoxLayout()
            hl.addWidget(QLabel("Falownik {}:".format(i + 1)), 1)
            hl.addWidget(cbInv, 1)
            hl.addWidget(cbPort, 1)
            layout.addLayout(hl)
            
        buttonBox = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Cancel)

        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)


        layout.addWidget(buttonBox)
        self.setLayout(layout)


        if self.exec_() == QDialog.Accepted:
            self.invList = dict()
            
            i = 0
            for cbInv in cbList.values():
                item  = invChooseList[cbInv.currentIndex()]
                
                if item.get("obj") is None:
                    continue
                #print("SelfExe")
                portList = cbInv.cbPort.portList
                #try:
                if portList is not None:
                    port = portList[cbInv.cbPort.currentIndex()].get("port")
                else:
                    port = None
                #except:
                    #pass
                #    port = "COM3"

                self.invList[i] = dict(item, port = port)
                
                i += 1
            
            return

    def __cbChanged(self, val):
        cb  = self.sender().cbPort
        obj = invChooseList[val].get("obj")
        
        cb.clear()
        
        if obj is not None:
            portList  = obj.getPortList()
            
            if portList is not None:
                for p in portList.values():
                    cb.addItem(p.get("title"))
                
                cb.portList = portList
                cb.setEnabled(True)
                return
        
        cb.portList = None
        cb.setEnabled(False)


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
		
        self.layout = QHBoxLayout()

        invList = InvChooser().invList
        self.invFrontendList  = dict()
        
        for item in invList.values():
            invCtrl = item["obj"](port = item["port"])
            invFrontend = InvFrontend(invCtrl, title = item["title"])
            self.layout.addWidget(invFrontend)

            self.invFrontendList[len(self.invFrontendList)] = invFrontend

        self.widget = QWidget()
        self.widget.setLayout(self.layout)
        self.setCentralWidget(self.widget)



        menuBar = self.menuBar()
        self.statusbar = self.statusBar()
        self.statusbar.showMessage('Ready')

        app = menuBar.addMenu("&File")
        newConfig = QAction("New configuration", self)
        app.addAction(newConfig)

        newConfig = QAction("Save", self)
        app.addAction(newConfig)

        newConfig = QAction("Exit", self)
        newConfig.triggered.connect(self.__closeApp)
        app.addAction(newConfig)

        macroMenu = menuBar.addMenu("&Macros")
        testsMenu = menuBar.addMenu("&Tests")

        torqeAngleTest = QAction("Torque angle", self)
        torqeAngleTest.triggered.connect(self.__runTorqueAngleTest)
        torqeAngleTest.setChecked(False)
        testsMenu.addAction(torqeAngleTest)

        actMacroMap = QAction("Efficiency map", self)
        actMacroMap.triggered.connect(self.__runMapMacro)
        macroMenu.addAction(actMacroMap)

        self.setWindowTitle("Main menu")
        self.show()

    def __runMapMacro(self):        
        MacroMapDialog(self.invFrontendList)

    def __runTorqueAngleTest(self):
        TorqueAngleConfig(self.invFrontendList)

    def __closeApp(self):
        sys.exit()

app = QApplication(sys.argv)
w   = MainWindow()
sys.exit(app.exec_())
