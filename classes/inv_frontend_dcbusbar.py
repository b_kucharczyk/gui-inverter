from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QHBoxLayout, QGroupBox, QLabel

#
# InvDcBusBar -- komponent wskaźnik szyny zasilającej
#

class InvDcBusBar(QGroupBox):
    def __init__(self, invControl, **kwargs):
        super().__init__()
        print(kwargs)
        self.invControl = invControl
        self.setTitle(kwargs.get("title"))
        
        self.regVoltage  = kwargs.get("volt_reg")
        self.regCurrent  = kwargs.get("curr_reg")
        
        self.decsVoltage = kwargs.get("volt_decs")
        self.decsCurrent = kwargs.get("curr_decs")
        
        self.valVoltage  = None
        self.valCurrent  = None
        
        f = QFont("Helvetica")
        f.setPointSize(f.pointSize() + 1)
        f.setBold(True)

        self.lbVoltage  = QLabel()
        self.lbVoltage.setAlignment(Qt.AlignVCenter | Qt.AlignCenter)
        self.lbVoltage.setFont(f)

        self.lbCurrent  = QLabel()
        self.lbCurrent.setAlignment(Qt.AlignVCenter | Qt.AlignCenter)
        self.lbCurrent.setFont(f)

        self.lbPower    = QLabel()
        self.lbPower.setAlignment(Qt.AlignVCenter | Qt.AlignCenter)
        self.lbPower.setFont(f)

        self.__lbUpdate()

        layout = QHBoxLayout()
        layout.addWidget(self.lbVoltage)
        layout.addWidget(self.lbCurrent)
        layout.addWidget(self.lbPower)


        self.setLayout(layout)

    def __lbUpdate(self):

        if self.valVoltage is not None:
            self.lbVoltage.setText("{val:.{decs}f} V".format(
                val   = self.valVoltage,
                decs  = self.decsVoltage))
        else:
            self.lbVoltage.setText("-- V")

        if self.valCurrent is not None:
            self.lbCurrent.setText("{val:.{decs}f} A".format(
                val   = self.valCurrent,
                decs  = self.decsCurrent))
        else:
            self.lbCurrent.setText("-- A")

        if self.valVoltage is not None and self.valCurrent is not None:
            self.lbPower.setText("{val:.{decs}f} W".format(
                val   = self.valVoltage * self.valCurrent,
                decs  = self.decsVoltage + self.decsCurrent))
        else:
            self.lbPower.setText("-- W")

    def regUpdate(self):

        self.valVoltage = float(self.invControl.getReg(self.regVoltage)) \
                        * pow(10, -self.decsVoltage)

        self.valCurrent = float(self.invControl.getReg(self.regCurrent)) \
                        * pow(10, -self.decsCurrent)

        self.__lbUpdate()

