from PyQt5.QtCore import Qt, QTimer
from PyQt5.QtGui import QPalette
from PyQt5.QtWidgets import QDialog, QDialogButtonBox, \
    QVBoxLayout, QGridLayout, QGroupBox, QLabel, QDoubleSpinBox, QPushButton, QProgressBar, QHBoxLayout

from PyQt5.QtWidgets import QMessageBox
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from classes.chart_classes.inv_frontend_charts import TestCharts

class TorqueAngleConfig(QDialog):

    def __init__(self, listInvFrontend):
        super().__init__()

        self.setWindowTitle("Torque angle test")
        self.numOfModbusRTU = None
        self.invFront = None
        self.driveOnViaTest = False
        self.step = 0
        self.setCurrent = 0
        self.setEncValue = 0
        self.startAutomaticTest = False
        self.elapsedTime = 0
        self.testTime = 0
        self.cycleTime = 10

        if len(listInvFrontend.values()) < 1:
            QMessageBox.warning(self, "Error", "At least one inverter is required to run the test.")
            return

        for i in range(len(listInvFrontend.values())):
            if listInvFrontend[i].title() == "Modbus RTU":
                self.numOfModbusRTU = i
                self.invFront = listInvFrontend[i]
        if self.numOfModbusRTU == None:
            QMessageBox.warning(self, "Error", "Inverter not found.")
            return

        layout = QVBoxLayout()
        layout2 = QVBoxLayout()
        layoutStatus = QVBoxLayout()

        box = QGroupBox()
        box.setTitle("Config")
        boxLayout = QGridLayout()

        # min - step - max
        for i, capt in {1: "min", 2: "krok", 3: "max"}.items():
            label = QLabel(capt)
            label.setMinimumWidth(50)
            label.setAlignment(Qt.AlignVCenter | Qt.AlignCenter)
            boxLayout.addWidget(label, 0, i)

        # prad
        self.currMin = QDoubleSpinBox()
        self.currMin.setKeyboardTracking(False)
        self.currMin.setAlignment(Qt.AlignRight)
        self.currMin.setRange(0, 1)
        self.currMin.setValue(0.0)
        self.currMin.setSingleStep(0.01)
        self.currMin.valueChanged.connect(self.updateTime)

        self.currMax = QDoubleSpinBox()
        self.currMax.setKeyboardTracking(False)
        self.currMax.setAlignment(Qt.AlignRight)
        self.currMax.setRange(0, 1)
        self.currMax.setValue(0.1)
        self.currMax.setSingleStep(0.01)
        self.currMax.valueChanged.connect(self.updateTime)

        self.currStep = QDoubleSpinBox()
        self.currStep.setKeyboardTracking(False)
        self.currStep.setAlignment(Qt.AlignRight)
        self.currStep.setRange(0, 1)
        self.currStep.setValue(0.01)
        self.currStep.setSingleStep(0.01)
        self.currStep.valueChanged.connect(self.updateTime)

        boxLayout.addWidget(
            QLabel("{} \nPrad Id ".format(listInvFrontend[self.numOfModbusRTU].title())), 1, 0)

        boxLayout.addWidget(self.currMin, 1, 1)
        boxLayout.addWidget(self.currStep, 1, 2)
        boxLayout.addWidget(self.currMax, 1, 3)

        # EncAngle
        self.angleMin = QDoubleSpinBox()
        self.angleMin.setKeyboardTracking(False)
        self.angleMin.setAlignment(Qt.AlignRight)
        self.angleMin.setSingleStep(0.1)
        self.angleMin.setRange(0, 1)
        self.angleMin.setValue(0)
        self.angleMin.valueChanged.connect(self.updateTime)

        self.angleMax = QDoubleSpinBox()
        self.angleMax.setKeyboardTracking(False)
        self.angleMax.setAlignment(Qt.AlignRight)
        self.angleMax.setSingleStep(0.1)
        self.angleMax.setRange(0, 1)
        self.angleMax.setValue(1)
        self.angleMax.valueChanged.connect(self.updateTime)

        self.angleStep = QDoubleSpinBox()
        self.angleStep.setKeyboardTracking(False)
        self.angleStep.setAlignment(Qt.AlignRight)
        self.angleStep.setRange(0, 1)
        self.angleStep.setSingleStep(0.01)
        self.angleStep.setValue(0.01)
        self.angleStep.valueChanged.connect(self.updateTime)

        boxLayout.addWidget(
            QLabel("{} \nEnc angle".format(listInvFrontend[self.numOfModbusRTU].title())), 2, 0)

        boxLayout.addWidget(self.angleMin, 2, 1)
        boxLayout.addWidget(self.angleStep, 2, 2)
        boxLayout.addWidget(self.angleMax, 2, 3)

        # time step
        self.timeStep = QDoubleSpinBox()
        self.timeStep.setKeyboardTracking(False)
        self.timeStep.setAlignment(Qt.AlignRight)
        self.timeStep.setRange(0.1, 60.0)
        self.timeStep.setValue(0.5)
        self.timeStep.setSingleStep(0.1)
        self.timeStep.valueChanged.connect(self.updateTime)


        boxLayout.addWidget(QLabel("Czas między \nkrokami [s]"), 3, 0)
        boxLayout.addWidget(self.timeStep, 3, 2)

        box.setLayout(boxLayout)

        boxBtn = QGroupBox()
        boxBtn.setTitle("Control")

        btnStart = QPushButton("Start")
        btnStart.clicked.connect(self.startTest)

        btnStop = QPushButton("Stop")
        btnStop.clicked.connect(self.stopTest)

        btnResetError = QPushButton("Error reset")
        btnResetError.clicked.connect(self.errorReset)

        boxStatus = QGroupBox()
        boxStatus.setTitle("Status")

        self.labelTime = QLabel("Estimated test time:")
        self.labelState = QLabel("Test state")

        self.progressBar = QProgressBar()
        self.progressBar.setAlignment(Qt.AlignCenter)
        self.labelElapsedTime = QLabel("Elapsed time: 0 s")

        layoutStatus.addWidget(self.labelTime)
        layoutStatus.addWidget(self.labelState)
        layoutStatus.addWidget(self.progressBar)
        layoutStatus.addWidget(self.labelElapsedTime)

        layout2.addWidget(btnStart)
        layout2.addWidget(btnStop)
        layout2.addWidget(btnResetError)

        boxBtn.setLayout(layout2)
        boxStatus.setLayout(layoutStatus)

        layout.addWidget(box)
        layout.addWidget(boxBtn)
        layout.addWidget(boxStatus)

        tmr = QTimer(self)
        tmr.timeout.connect(self.__updateWidgets)
        tmr.start(200)

        automaticTimer = QTimer(self)
        automaticTimer.timeout.connect(self.__automaticTest)
        automaticTimer.start(self.cycleTime)

        self.testChartLayout = TestCharts()
        mainLayout = QHBoxLayout()
        mainLayout.addLayout(layout)
        mainLayout.addLayout(self.testChartLayout)

        self.updateTime()
        self.setLayout(mainLayout)
        #if self.exec_() == QDialog.Accepted:
           # pass
        self.exec_()

    def __updateChart(self):
        self.testChartLayout.updateChart()

    def __updateWidgets(self):

        if self.invFront.invControl.getReg("MB_Inverter_Power") != 0:
            self.labelState.setText("Test state: ERROR DRIVE")

        elif self.driveOnViaTest:
            self.progressBar.setValue(self.testTime)
            self.labelElapsedTime.setText("Elapsed time: " + str(round(self.testTime)) + " s")

            if self.step == 1:
                self.labelState.setText("Test state: WAITING FOR INVERTER ENABLE")
            else:
                self.labelState.setText("Test state: TEST STARTED")
                self.testChartLayout.updateValue(self.setEncValue, self.setCurrent)

        else:

            self.progressBar.setValue(0)
            self.labelState.setText("Test state: WAITING FOR START")

    def updateTime(self):
        try:
            timeEstimated = round(((((self.currMax.value() - self.currMin.value()) / self.currStep.value())+1) * self.timeStep.value()) \
            * ((((self.angleMax.value() - self.angleMin.value()) / self.angleStep.value()))+1))
            self.progressBar.setRange(0, timeEstimated)
            self.labelTime.setText("Estimated test time: " + str(timeEstimated) + " s")

        except:
            self.progressBar.setRange(0, 0)
            self.labelTime.setText("Estimated test time: INDEFINITE")


    def __automaticTest(self):

        #print("Step: " + str(self.step) + " . Set encValue: " + str(self.setEncValue) + " Set current: " + str(self.setCurrent))
        if self.step > 1:
            self.testTime += self.cycleTime / 1000
        else:
            self.testTime = 0

        if self.step == 0: # Wait for start
            if self.startAutomaticTest:
                self.startAutomaticTest = False
                self.step = 1

        elif self.step == 1: # Check the motor is start
            if self.invFront.invControl.getReg("MB_Inverter_State") == 3:
                self.step = 2

        elif self.step == 2: # Set the first current
            self.setCurrent = self.currMin.value()
            self.setEncValue = self.angleMin.value()
            self.setAngleAndCurrent(True, True)
            self.elapsedTime = self.cycleTime
            self.step = 3

        elif self.step == 3: # Wait time
            self.elapsedTime += self.cycleTime

            if self.elapsedTime >= (self.timeStep.value()*1000):
                self.step = 4

        elif self.step == 4: # Increase the encValue and come back to 3. If encValue >= max setZero
            self.setEncValue += self.angleStep.value()

            if self.setEncValue <= self.angleMax.value():
                self.setAngleAndCurrent(True, False)
                self.elapsedTime = self.cycleTime
                self.step = 3

            else:
                self.step = 5

        elif self.step == 5: # Increases current Id, decrease encValue
            self.setCurrent += self.currStep.value()
            self.setEncValue = self.angleMin.value()

            if self.setCurrent <= self.currMax.value():
                self.elapsedTime = self.cycleTime
                self.setAngleAndCurrent(True, True)
                self.step = 3

            else:
                self.setCurrent = 0
                self.setEncValue = 0
                self.setAngleAndCurrent(True, True)
                self.step = 6

        elif self.step == 6: # end of test
            self.step = 0
            self.elapsedTime = 0
            self.driveOnViaTest = False
            self.turnOffInverter()
            self.turnOnOffWidgets(True)

    def setAngleAndCurrent(self, angle, current):
        if angle:
            self.invFront.invControl.setReg("MB_AS_var", self.setEncValue)

        if current:
            self.invFront.invControl.setReg("MB_SP_Id", self.setCurrent)

    def startTest(self):
        self.turnOnOffWidgets(False)

        # Sprawdzenie warunku rozpoczecia testu - brak bledu:
        if self.invFront.invControl.getReg("MB_Inverter_Power") != 0:
            self.stopTest()
            return

        # Sprawdzenie zalaczenia napedu
        if self.invFront.invControl.getReg("MB_Inverter_State") == 0: # Jak iddle to zalacz naped
            self.turnOnInverter()
            self.testChartLayout.newData()
            self.driveOnViaTest = True
            self.startAutomaticTest = True

        else:
            # W przeciwnym razie nie rob nic. Naped moze jechac. Nie mozna uruchomic procedury
            # jak naped jest juz w ruchu. Moze byc inny nadzor / test wlaczony.
            self.stopTest()
            return


    def stopTest(self):
        if self.driveOnViaTest:
            self.driveOnViaTest = False
            self.step = 6
            self.turnOffInverter()

        self.turnOnOffWidgets(True)

    def errorReset(self):
        self.invFront.invControl.setReg("MB_Command", 4)


    def turnOnInverter(self):
        self.invFront.invControl.setReg("MB_Command", 2)

    def turnOffInverter(self):
        self.invFront.invControl.setReg("MB_Command", 1)


    def turnOnOffWidgets(self, state):

        if state:
            self.timeStep.setEnabled(True)
            self.angleMin.setEnabled(True)
            self.angleStep.setEnabled(True)
            self.angleMax.setEnabled(True)
            self.currMin.setEnabled(True)
            self.currMax.setEnabled(True)
            self.currStep.setEnabled(True)
        else:
            self.timeStep.setEnabled(False)
            self.angleMin.setEnabled(False)
            self.angleStep.setEnabled(False)
            self.angleMax.setEnabled(False)
            self.currMin.setEnabled(False)
            self.currMax.setEnabled(False)
            self.currStep.setEnabled(False)


chartConfig = {
    "chart1": {
           "title":        "Set value",
           "numOfCharts":  2,
           "readSignals:": {
               "1":    "EncSet",
               "2":    "CurrentSet"
           },
           "updateTime":   2000
    },
    "chart2": {
           "title": "Set value",
           "numOfCharts": 2,
           "readSignals:": {
               "1": "EncSet",
               "2": "CurrentSet"
           },
           "updateTime": 2000
    }}


