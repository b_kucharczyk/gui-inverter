import matplotlib.pyplot as plt
from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QVBoxLayout, QGroupBox
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas

class TestCharts(QVBoxLayout):

    def __init__(self, **kwargs):
        super().__init__()

        self.line1Data = []
        self.line2Data = []

        box = QGroupBox()
        box.setTitle("Charts")

        # Chart
        chartLayout = QVBoxLayout()

        self.figure = plt.figure()
        self.figure.patch.set_facecolor("#F0F0F0")
        self.canvas = FigureCanvas(self.figure)
       # cid = self.canvas.mpl_connect('button_press_event', self.onclick)

        chartLayout.addWidget(self.canvas)
        box.setLayout(chartLayout)

        self.addWidget(box)
        self.__drawChart()

        tmr = QTimer(self)
        tmr.timeout.connect(self.__updateChart)
        tmr.start(2000)

    def __updateChart(self):
        self.figure.clear()
        self.__drawChart()
        self.canvas.draw()

    def __drawChart(self):
        ax = self.figure.add_subplot(211)
        ay = self.figure.add_subplot(212)

        ax.set(ylabel='Enc set',
               title='Set value')


        ax.grid()
        ay.set(xlabel='sample', ylabel='Current set')
        ay.grid()
        ax.plot(self.line1Data, 'b-')
        ay.plot(self.line2Data, 'b-')

    def updateValue(self, line1, line2):
       self.line1Data.append(line1)
       self.line2Data.append(line2)

    def newData(self):
        self.line1Data = []
        self.line2Data = []

   # def onclick(self,event):
   #     print('%s click: button=%d, x=%d, y=%d, xdata=%f, ydata=%f' %
   #           ('double' if event.dblclick else 'single', event.button,
   #            event.x, event.y, event.xdata, event.ydata))
