from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QHBoxLayout, QGroupBox, QLabel


# Author: Bartlomiej Kucharczyk
# Date: 07.08.2020
# Class name: InvTemp
# Desc: This class create layout for 3 temp value


class InvTemp(QGroupBox):
    def __init__(self, invControl, **kwargs):
        super().__init__()
        print(kwargs)
        self.invControl = invControl
        self.setTitle(kwargs.get("title"))

        self.regTemp1 = kwargs.get("temp1_regs")
        self.regTemp2 = kwargs.get("temp2_regs")
        self.regTemp3 = kwargs.get("temp3_regs")

        self.descTemp1 = kwargs.get("temp1_decs")
        self.descTemp2 = kwargs.get("temp2_decs")
        self.descTemp3 = kwargs.get("temp3_decs")

        f = QFont("Helvetica")
        f.setPointSize(f.pointSize() + 1)
        f.setBold(True)

        self.valTemp1 = None
        self.valTemp2 = None
        self.valTemp3 = None

        self.lbTemp1 = QLabel()
        self.lbTemp1.setAlignment(Qt.AlignCenter | Qt.AlignCenter)
        self.lbTemp1.setFont(f)

        self.lbTemp2 = QLabel()
        self.lbTemp2.setAlignment(Qt.AlignCenter | Qt.AlignCenter)
        self.lbTemp2.setFont(f)

        self.lbTemp3 = QLabel()
        self.lbTemp3.setAlignment(Qt.AlignCenter | Qt.AlignCenter)
        self.lbTemp3.setFont(f)

        self.__lbUpdate()

        layout = QHBoxLayout()
        layout.addWidget(self.lbTemp1)
        layout.addWidget(self.lbTemp2)
        layout.addWidget(self.lbTemp3)
        self.setLayout(layout)


    def __lbUpdate(self):
        if self.valTemp1 is not None:
            self.lbTemp1.setText("{val:.{decs}f} \N{DEGREE SIGN}C".format(
                val=self.valTemp1,
                decs=self.descTemp1))
        else:
            self.lbTemp1.setText("-- \N{DEGREE SIGN}C")

        if self.valTemp2 is not None:
            self.lbTemp2.setText("{val:.{decs}f} \N{DEGREE SIGN}C".format(
                val=self.valTemp2,
                decs=self.descTemp2))
        else:
            self.lbTemp2.setText("-- \N{DEGREE SIGN}C")

        if self.valTemp3 is not None:
            self.lbTemp3.setText("{val:.{decs}f} \N{DEGREE SIGN}C".format(
                val=self.valTemp3,
                decs=self.descTemp3))
        else:
            self.lbTemp3.setText("-- \N{DEGREE SIGN}C")



    def regUpdate(self):
        self.valTemp1 = float(self.invControl.getReg(self.regTemp1)) \
                          * pow(10, -self.descTemp1)

        self.valTemp2 = float(self.invControl.getReg(self.regTemp2)) \
                        * pow(10, -self.descTemp2)

        self.valTemp3 = float(self.invControl.getReg(self.regTemp3)) \
                        * pow(10, -self.descTemp3)

        self.__lbUpdate()
