from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QHBoxLayout, QGroupBox, QLabel, QRadioButton, QVBoxLayout, QGridLayout
import collections

#
# InvWarningBar -- komponent wskaźnik aktualnych bledow
#

class InvWarningBar(QGroupBox):
    def __init__(self, invControl, **kwargs):
        super().__init__()

        self.invControl = invControl
        self.setTitle(kwargs.get("title"))
        self.regName = kwargs.get("reg")

        layout = QHBoxLayout()
        layout.setAlignment(Qt.AlignTop)

        self.labelSet = collections.OrderedDict()

        for title, mask in kwargs.get("flags").items():
            qRadioBtn = QRadioButton(title)
            qRadioBtn.setAutoExclusive(False)
            layout.addWidget(qRadioBtn)


            self.labelSet[mask] = qRadioBtn

        self.setLayout(layout)

    def regUpdate(self):
        val = self.invControl.getReg(self.regName)
        for mask, label in self.labelSet.items():
           label.setChecked(bool(int(val) & mask))
