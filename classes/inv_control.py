import serial.tools.list_ports
import minimalmodbus
import ctypes as ct

class InvControl:
    def __init__(self, **kwargs):
        self.regsArray = [0.0] * 48
        self.goodFramesCount  = 0
        self.badFramesCount   = 0

        self.connected = False

        try:
            self.port = kwargs["port"]
            self.instrument = minimalmodbus.Instrument("COM3", 9)
            self.instrument.serial.baudrate = 9600
            self.instrument.serial.timeout = 0.1
            self.instrument.serial.bytesize = 8
            self.instrument.serial.parity = serial.PARITY_NONE
            self.instrument.serial.stopbits = 1
            self.connected = True
        except:
            self.connected = False
            self.badFramesCount += 1

    def getMbusData(self):
        try:

            valMBus = self.instrument.read_registers(0, 30, 3)
            #valMBus2 = float(self.instrument.read_registers(0, 30, 3))
            #print(valMBus)
            for i in range(len(valMBus)):
                if i == 7 or i == 8 or i == 12 or i == 13 or i == 14 or i == 19: #W i == 16 or i == 17 or i == 18 or i == 15
                    try:
                        #print("I" + str(i) + " Val:" + str(valMBus[i].value))
                        self.regsArray[i] = ct.c_int16(valMBus[i])/32767.0
                    except:
                        #print(i)
                        self.regsArray[i] = (valMBus[i])/32767.0 # 0
                else:
                    self.regsArray[i] = valMBus[i]

            #print(self.regsArray)
        except:
            self.badFramesCount += 1

    def getReg(self, name):
        if not name in self.registerDefs:
          #  print("get nonexist reg \"{}\"".format(name))
            return None

        addr  = self.registerDefs[name]
        val   = self.regsArray[addr - 4001]

        # print(self.regsArray)
        # print("get reg \"{}\" = {}".format(name, val))
        return val

    def setReg(self, name, val):
        if not name in self.registerDefs:
            print("set nonexist reg \"{}\"".format(name))
            return None

        addr  = self.registerDefs[name]
        self.regsArray[addr - 4001]  = int(val)
        #print("set reg \"{}\" = {}".format(name, int(val)))

        if name == "MB_SP_Iq":
            self.instrument.write_register(38, val*32767, signed=True)
        elif name == "MB_SP_Id":
            self.instrument.write_register(39, val*32767, signed=True)

        elif name == "MB_SP_Speed":
            self.instrument.write_register(41, val*32767, signed=True)
        elif name == "MB_Command":
            self.instrument.write_register(30, int(val))
        elif name == "MB_SP_PosMech":
            self.instrument.write_register(32, int(val), signed=True)
        elif name == "MB_Dac1Cfg":
            self.instrument.write_register(34, int(val))
        elif name == "MB_Dac2Cfg":
            self.instrument.write_register(35, int(val))
        elif name == "MB_Dac3Cfg":
            self.instrument.write_register(36, int(val))
        elif name == "MB_Dac4Cfg":
            self.instrument.write_register(37, int(val))
        elif name == "MB_AngleSource":
            self.instrument.write_register(31, int(val))
        elif name == "MB_AS_var":
            self.instrument.write_register(32, val*32767, signed=True)
        elif name == "MB_AS_RampGen_f":
            self.instrument.write_register(33, int(val))


        self.goodFramesCount += 1
        return True

    def readStatusRegs(self):
        self.goodFramesCount += 1

    def getConnType(self):
            return "port "+ self.port +" is open" #"Modbus RTU "+ self.port

    def getGoodFramesCount(self):
        return self.goodFramesCount

    def getBadFramesCount(self):
        return self.badFramesCount

    @staticmethod
    def getPortList():
        # KB - wczytanie dostepnych portow
        ports = serial.tools.list_ports.comports()
        portList = dict()

        for p in range(len(ports)):
            print(ports[p])
            portList[p] = dict(
                port  = ports[p].device,
                title = str(ports[p].device)
            )

        return portList

    registerDefs = {
        "MB_Global_Status":     4001,
        "MB_Inverter_State":    4002,
        "MB_Inverter_Power":    4003,
        "MB_Inverter_Warnings": 4004,
        "MB_Act_Iq":            4008,
        "MB_Act_Id":            4009,
        "MB_Act_Speed":         4011,
        "MB_Act_PosRaw":        4012,
        "MB_Act_PosMech":       4013,
        "MB_Act_PosElec":       4014,
        "MB_Act_ElThetaPark":   4015,
        "MB_Act_DcBus_U":       4016,
        "MB_Act_DcBus_I":       4017,
        "MB_Act_phUIrms":       4018,
        "MB_Act_phVIrms":       4019,
        "MB_Act_phWIrms":       4019,
        "MB_Act_Temp1":         4021,
        "MB_Act_Temp2":         4022,
        "MB_Act_Temp3":         4023,
        "MB_Poles":             4028,
        "MB_Command":           4030,
        "MB_InverterMode":      4031,
        "MB_AngleSource":       4031,
        "MB_AS_var":            4032,
        "MB_AS_RampGen_f":      4033,
        "MB_Dac1Cfg":           4034,
        "MB_Dac2Cfg":           4035,
        "MB_Dac3Cfg":           4036,
        "MB_Dac4Cfg":           4037,
        "MB_SP_Iq":             4036,
        "MB_SP_Id":             4037,
        "MB_SP_Speed":          4038,
        "MB_SP_PosMech":        4039,
        "MB_SP_EncOffset":      4043,
    }
    
    frontendCaps = {
        "command": {
            "title":    "Command",
            "type":     "barCommand",
            "reg":      "MB_Command",
            "btn_grp": {
                "Power": {
                    "Enable":   0x02,
                    "Disable":  0x01
                },
                "Reset": {
                    "Reset Errors": 0x04
                }
            }
        },
        "config": {
            "title":    "Config",
            "type":     "barConfig",
            "configs": {
                "Mode": {
                    "reg":      "MB_InverterMode",
                    "choose": {
                        0: "Torque",
                        1: "Speed",
                        2: "Position"
                    },
                    "readonly": True
                },
                "Poles   ": {
                    "reg":      "MB_Poles",
                    "readonly": True
                },
                "Enc Offset": {
                    "reg":      "MB_SP_EncOffset",
                    "min":         0,
                    "max":      1024,
                    "readonly": True
                }
            }
        },
        "encSet": {
            "title": "Encoder options",
            "type": "barEncoSet",
            "configs": {
                "Mode": {
                    "reg": "MB_AngleSource",
                    "choose": {
                        0: "Qep",
                        1: "Var",
                        2: "RampGen"
                    },
                    "readonly": False
                },
                "AS_var    ": {
                    "reg": "MB_AS_var",
                    "min": 0,
                    "max": 1,
                    "readonly": False
                },
                "AS_RampGen_f": {
                    "reg": "MB_AS_RampGen_f",
                    "min": 0,
                    "max": 1000,
                    "readonly": False
                }


            }
        },
        "status": {
            "title":    "Status",
            "type":     "barStatus",
            "reg":      "MB_Inverter_State",#MB_Global_Status
            "flags": {
                "Status":   0x01,
                "Drv Err":  0x20,
                "DC OC":    0x10,
                "DC OV":    0x40
            }
        },
        "err_bus": {
            "title": "Errors",
            "type": "barErrors",
            "reg": "MB_Inverter_Power",
            "flags": {
                "OverCurrentTrip_IL11": 0x01,
                "OverCurrentTrip_IL21": 0x02,
                "OverCurrentTrip_IL31": 0x04,
                "OverVoltageDCTrip1"  : 0x08,
                "DriverTrip_11"       : 0x10,
                "DriverTrip_21"       : 0x20,
                "DriverTrip_31"       : 0x40,
                "CommErrorTrip1"      : 0x80,
                "TempTrip_11"         : 0x100,
                "TempTrip_21"         : 0x200,
                "TempTrip_31"         : 0x400,
                "rsvd"                : 0x800
            }
        },
        "warr_bus": {
            "title": "Warnings",
            "type": "barWarnings",
            "reg": "MB_Inverter_Warnings",
            "flags": {
                "EncNoldx:1": 0x01,
                "LowUdc": 0x02,
            }
        },
        "iq": {
            "title":    "Torque/Iq",
            "type":     "barSetpoint",
            "reg":      "MB_SP_Iq",
            "act_reg":  "MB_Act_Iq",
            "unit":     "A",
            "min":      -1,
            "max":       1,
            "decs":      3
        },
        "id": {
            "title":    "Flux/Id",
            "type":     "barSetpoint",
            "reg":      "MB_SP_Id",
            "act_reg":  "MB_Act_Id",
            "unit":     "A",
            "min":      -1,
            "max":       1,
            "decs":      3
        },
        "vel": {
            "title":    "Velocity",
            "type":     "barSetpoint",
            "reg":      "MB_SP_Speed",
            "act_reg":  "MB_Act_Speed",
            "unit":     "RPM",
            "min":      -1800,
            "max":       1800,
            "decs":         0
        },
        "pos": {
            "title":    "Position",
            "type":     "barSetpoint",
            "reg":      "MB_SP_PosMech",
            "act_reg":  "MB_Act_PosMech",
            "unit":     "\N{DEGREE SIGN}",
            "min":      -180,
            "max":       180,
            "decs":        1
        },
        "dc_bus": {
            "title":        "DC Bus",
            "type":         "barDcBus",
            "volt_reg":     "MB_Act_DcBus_U",
            "volt_decs":    0,
            "curr_reg":     "MB_Act_DcBus_I",
            "curr_decs":    1
        },
        "temp_bus": {
            "title": "Temperature",
            "type": "barTemp",
            "unit": "\N{DEGREE SIGN}C",
            "temp1_regs": "MB_Act_Temp1",
            "temp1_decs": 1,
            "temp2_regs": "MB_Act_Temp2",
            "temp2_decs": 1,
            "temp3_regs": "MB_Act_Temp3",
            "temp3_decs": 1
            },
        "enc_bus": {
            "title": "Encoder",
            "type": "barEnc",
            "unit": "\N{DEGREE SIGN}c",
            "enc_QepRaw": "MB_Act_PosRaw",
            "enc_QepRaw_decs": 0,
            "enc_PosMech": "MB_Act_PosMech",
            "enc_PosMech_decs": 3,
            "enc_PosElec": "MB_Act_PosElec",
            "enc_PosElec_decs": 3,
            "enc_ElThetaPark": "MB_Act_ElThetaPark",
            "enc_ElThetaPark_decs": 3
        },
        "irms_bus": {
            "title": "Irms",
            "type": "barIrms",
            "unit": "pu",
            "phU_Irms": "MB_Act_phUIrms",
            "phU_Irms_decs": 2,
            "phV_Irms": "MB_Act_phVIrms",
            "phV_Irms_decs": 2,
            "phW_Irms": "MB_Act_phWIrms",
            "phW_Irms_decs": 2
        },

        "dac_config": {
            "title":        "DACs",
            "type":         "barDac",
            "regs": {
                "DAC 1":    "MB_Dac1Cfg",
                "DAC 2":    "MB_Dac2Cfg",
                "DAC 3":    "MB_Dac3Cfg",
                "DAC 4":    "MB_Dac4Cfg",
            },
            "choose": {
                 0: "None",
                 1: "pi_iq_Ref",
                 2: "pi_iq_Fdb",
                 3: "pi_iq_Out",
                 4: "pi_id_Ref",
                 5: "pi_id_Fdb",
                 6: "pi_id_Out",
                 7: "pi_spd_Ref",
                 8: "pi_spd_Fdb",
                 9: "pi_spd_Out",
                10: "i_u",
                11: "i_v",
                12: "i_w",
                13: "e_dac_enc_mech",
                14: "e_dac_enc_el",
                15: "e_dac_theta_park",
                16: "dcbus_i",
                17: "dcbus_u",
            }
        }
    }
