from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QHBoxLayout, QGroupBox, QLabel, QRadioButton, QVBoxLayout, QGridLayout
import collections

#
# InvErrorBar -- komponent wskaźnik aktualnych bledow
#

class InvErrorBar(QGroupBox):
    def __init__(self, invControl, **kwargs):
        super().__init__()

        self.invControl = invControl
        self.setTitle(kwargs.get("title"))
        self.regName = kwargs.get("reg")

        grid_layout = QGridLayout()
        row = 0
        col = 0

        self.labelSet = collections.OrderedDict()

        for title, mask in kwargs.get("flags").items():
            label = QRadioButton(title)
            #label.setCheckable(False)
            label.setAutoExclusive(False)
            grid_layout.addWidget(label,col,row)


            col += 1
            if col > 3:
                row += 1
                col = 0

            self.labelSet[mask] = label
        """
        self.overCurrentTrip1l = QRadioButton("OverCurrentTrip_IL1:1")
        self.overCurrentTrip2l = QRadioButton("OverCurrentTrip_IL2:1")
        self.overCurrentTrip3l = QRadioButton("OverCurrentTrip_IL3:1")

        self.overCurrentTrip1l.setCheckable(False)
        self.overCurrentTrip2l.setCheckable(False)
        self.overCurrentTrip3l.setCheckable(False)

        self.driveTrip1 = QRadioButton("DriverTrip_1:1")
        self.driveTrip2 = QRadioButton("DriverTrip_2:1")
        self.driveTrip3 = QRadioButton("DriverTrip_3:1")

        self.driveTrip1.setCheckable(False)
        self.driveTrip2.setCheckable(False)
        self.driveTrip3.setCheckable(False)

        self.tempTrip1 = QRadioButton("TempTrip_1:1")
        self.tempTrip2 = QRadioButton("TempTrip_2:1")
        self.tempTrip3 = QRadioButton("TempTrip_3:1")

        self.tempTrip1.setCheckable(False)
        self.tempTrip2.setCheckable(False)
        self.tempTrip3.setCheckable(False)

        self.overVoltageDCTrip1 = QRadioButton("OverVoltageDCTrip:1")
        self.commErrorTrip1 = QRadioButton("CommErrorTrip1")
        self.commErrorTrip2 = QRadioButton("rsvd")
        #self.commErrorTrip2.setVisible(False)

        self.overVoltageDCTrip1.setCheckable(False)
        self.commErrorTrip1.setCheckable(False)
        self.commErrorTrip2.setCheckable(False)

        self.__lbUpdate()

        layoutV = QVBoxLayout()
        layout = QHBoxLayout()
        layout2 = QHBoxLayout()
        layout3 = QHBoxLayout()
        layout4 = QHBoxLayout()

        layout.addWidget(self.overCurrentTrip1l)
        layout.addWidget(self.overCurrentTrip2l)
        layout.addWidget(self.overCurrentTrip3l)
        #layout.setAlignment(Qt.AlignJustify)

        layout2.addWidget(self.driveTrip1)
        layout2.addWidget(self.driveTrip2)
        layout2.addWidget(self.driveTrip3)
        #layout2.setAlignment(Qt.AlignJustify)

        layout3.addWidget(self.tempTrip1)
        layout3.addWidget(self.tempTrip2)
        layout3.addWidget(self.tempTrip3)
        # layout3.setAlignment(Qt.AlignJustify)

        layout4.addWidget(self.overVoltageDCTrip1)
        layout4.addWidget(self.commErrorTrip1)
        layout4.addWidget(self.commErrorTrip2)
        #layout4.setAlignment(Qt.AlignLeft)

        
        layoutV.addLayout(layout)
        layoutV.addLayout(layout2)
        layoutV.addLayout(layout3)
        layoutV.addLayout(layout4)
        """

        self.setLayout(grid_layout)

    def regUpdate(self):
        val = self.invControl.getReg(self.regName)
        #print(val)
        for mask, label in self.labelSet.items():
           label.setChecked(bool(int(val) & mask))
