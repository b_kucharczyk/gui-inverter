from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QHBoxLayout, QGroupBox, QLabel, QVBoxLayout


# Author: Bartlomiej Kucharczyk
# Date: 17.08.2020
# Class name: InvEnc
# Desc: This class create layout for 3 temp value

class InvEnc(QGroupBox):
    def __init__(self, invControl, **kwargs):
        super().__init__()
        self.invControl = invControl
        self.setTitle(kwargs.get("title"))

        self.encRaw         = kwargs.get("enc_QepRaw")
        self.encPosMech     = kwargs.get("enc_PosMech")
        self.encPosElec     = kwargs.get("enc_PosElec")
        self.encElThetaPark = kwargs.get("enc_ElThetaPark")

        self.descEncRaw         = kwargs.get("enc_QepRaw_decs")
        self.descEncPosMech     = kwargs.get("enc_PosMech_decs")
        self.descEncPosElec     = kwargs.get("enc_PosElec_decs")
        self.descEncElThetaPark = kwargs.get("enc_ElThetaPark_decs")

        f = QFont("Helvetica")
        f.setPointSize(f.pointSize()+1) # f.pointSize()
        f.setBold(True)

        self.valEncRaw         = None
        self.valEncPosMech     = None
        self.valEncPosElec     = None
        self.valEncElThetaPark = None

        self.lbEncRaw = QLabel()
        self.lbEncRaw.setAlignment(Qt.AlignLeft | Qt.AlignLeft)
        self.lbEncRaw.setFont(f)

        self.lbEncPosMech = QLabel()
        self.lbEncPosMech.setAlignment(Qt.AlignLeft | Qt.AlignLeft)
        self.lbEncPosMech.setFont(f)

        self.lbEncPosElec = QLabel()
        self.lbEncPosElec.setAlignment(Qt.AlignLeft | Qt.AlignLeft)
        self.lbEncPosElec.setFont(f)

        self.lbEncElThetaPark = QLabel()
        self.lbEncElThetaPark.setAlignment(Qt.AlignLeft | Qt.AlignLeft)
        self.lbEncElThetaPark.setFont(f)

        self.__lbUpdate()

        layout = QHBoxLayout()
        layout.addWidget(self.lbEncPosElec)
        layout.addWidget(self.lbEncPosMech)

        layout2 = QHBoxLayout()
        layout2.addWidget(self.lbEncRaw)
        layout2.addWidget(self.lbEncElThetaPark)

        mainLayout = QVBoxLayout()
        mainLayout.addLayout(layout2)
        mainLayout.addLayout(layout)

        self.setLayout(mainLayout)


    def __lbUpdate(self):
        if self.valEncRaw is not None:
            self.lbEncRaw.setText("Raw:       {val:.{decs}f} \N{DEGREE SIGN}".format(
                val=self.valEncRaw,
                decs=self.descEncRaw))
        else:
            self.lbEncRaw.setText("Raw: -- \N{DEGREE SIGN}")

        if self.valEncPosMech is not None:
            self.lbEncPosMech.setText("PosMech: {val:.{decs}f} \N{DEGREE SIGN}".format(
                val=self.valEncPosMech,
                decs=self.descEncPosMech))
        else:
            self.lbEncPosMech.setText("PosMech: -- \N{DEGREE SIGN}")

        if self.valEncPosElec is not None:
            self.lbEncPosElec.setText("PosElec: {val:.{decs}f} \N{DEGREE SIGN}".format(
                val=self.valEncPosElec,
                decs=self.descEncPosElec))
        else:
            self.lbEncPosElec.setText("PosElec: -- \N{DEGREE SIGN}")

        if self.valEncElThetaPark is not None:
            self.lbEncElThetaPark.setText("Park:        {val:.{decs}f} \N{DEGREE SIGN}".format(
                val=self.valEncElThetaPark,
                decs=self.descEncElThetaPark))
        else:
            self.lbEncElThetaPark.setText("Park: -- \N{DEGREE SIGN}")

    def regUpdate(self):
        self.valEncRaw = float(self.invControl.getReg(self.encRaw)) \
                          #* pow(10, -self.descEncRaw)

        #print(self.invControl.getReg(self.encPosMech))
        self.valEncPosMech = float(self.invControl.getReg(self.encPosMech)) \
                        #* pow(10, -self.descEncPosMech)

        #print(self.invControl.getReg(self.encPosElec))

        self.valEncPosElec = float(self.invControl.getReg(self.encPosElec)) \
                        #* pow(10, -self.descEncPosElec)

        self.valEncElThetaPark = float(self.invControl.getReg(self.encElThetaPark)) \
                             #* pow(10, -self.descEncElThetaPark)

        self.__lbUpdate()
