from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDialog, QDialogButtonBox, \
        QVBoxLayout, QGridLayout, QGroupBox, QLabel, QDoubleSpinBox

from PyQt5.QtWidgets import QMessageBox
APP_NAME = ""
class MacroMapDialog(QDialog):
    def __init__(self, listInvFrontend):
        super().__init__()
        
        if len(listInvFrontend.values()) < 2:
            QMessageBox.warning(self, APP_NAME,
                "Do uruchomienia makra potrzebne są dwa falowniki.")
            return

        self.setWindowTitle(APP_NAME)
        
        layout = QVBoxLayout()
        
        box = QGroupBox()
        box.setTitle("Mapa sprawności")
        boxLayout = QGridLayout()
        
        #for inv in listInvFrontend.values():
        #   boxLayout.addWidget(QLabel(inv.title()))
        
        # min - step - max
        for i, capt in { 1: "min", 2: "krok", 3: "max" }.items():
            label = QLabel(capt)
            label.setAlignment(Qt.AlignVCenter | Qt.AlignCenter)
            boxLayout.addWidget(label, 0, i)
        
        # moment
        self.torqMin  = QDoubleSpinBox()
        self.torqMin.setKeyboardTracking(False)
        self.torqMin.setAlignment(Qt.AlignRight)
        self.torqMin.setRange(-100, 100)
        self.torqMin.setValue(0.0)
        
        self.torqMax  = QDoubleSpinBox()
        self.torqMax.setKeyboardTracking(False)
        self.torqMax.setAlignment(Qt.AlignRight)
        self.torqMax.setRange(-100, 100)
        self.torqMax.setValue(100)
        
        self.torqStep = QDoubleSpinBox()
        self.torqStep.setKeyboardTracking(False)
        self.torqStep.setAlignment(Qt.AlignRight)
        self.torqStep.setRange(0.01, 100)
        self.torqStep.setValue(10.0)

        boxLayout.addWidget(
            QLabel("{}\nmoment [%]".format(listInvFrontend[0].title())), 1, 0)
        
        boxLayout.addWidget(self.torqMin, 1, 1)
        boxLayout.addWidget(self.torqStep, 1, 2)
        boxLayout.addWidget(self.torqMax, 1, 3)

        # obroty
        self.velMin   = QDoubleSpinBox()
        self.velMin.setKeyboardTracking(False)
        self.velMin.setAlignment(Qt.AlignRight)
        self.velMin.setRange(-3000, 3000)
        self.velMin.setValue(0)
        
        self.velMax   = QDoubleSpinBox()
        self.velMax.setKeyboardTracking(False)
        self.velMax.setAlignment(Qt.AlignRight)
        self.velMax.setRange(-3000, 3000)
        self.velMax.setValue(3000)
        
        self.velStep  = QDoubleSpinBox()
        self.velStep.setKeyboardTracking(False)
        self.velStep.setAlignment(Qt.AlignRight)
        self.velStep.setRange(0.1, 3000)
        self.velStep.setValue(30)
        
        boxLayout.addWidget(
            QLabel("{}\nobroty [RPM]".format(listInvFrontend[1].title())), 2, 0)
        
        boxLayout.addWidget(self.velMin, 2, 1)
        boxLayout.addWidget(self.velStep, 2, 2)
        boxLayout.addWidget(self.velMax, 2, 3)

        # czas
        self.timeStep = QDoubleSpinBox()
        self.timeStep.setKeyboardTracking(False)
        self.timeStep.setAlignment(Qt.AlignRight)
        self.timeStep.setRange(0.1, 60.0)
        self.timeStep.setValue(1.0)
        
        boxLayout.addWidget(QLabel("czas między\nkrokami [s]"), 3, 0)
        boxLayout.addWidget(self.timeStep, 3, 2)
        
        box.setLayout(boxLayout)
        layout.addWidget(box)
        
        buttonBox = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Cancel)

        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)

        layout.addWidget(buttonBox)
        self.setLayout(layout)
        
        if self.exec_() == QDialog.Accepted:
            pass

